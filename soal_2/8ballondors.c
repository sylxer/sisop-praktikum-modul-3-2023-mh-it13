#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

#define MAX_BUFFER_SIZE 1024

// Fungsi download file
void downloadFile(const char *url) 
{
    system("wget -O original.txt https://drive.google.com/uc?id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW");
}

// Fungsi untuk memfilter file dari karakter non huruf dari original.txt ke dalam thebeatles.txt
void filterFile() 
{
    FILE *inputFile = fopen("original.txt", "r"); // membuka file original.txt pada mode baca "r"
    FILE *outputFile = fopen("thebeatles.txt", "w"); // membuka file original.txt pada mode tulis "w"

    if (inputFile == NULL || outputFile == NULL) // mengecek apakah file bisa dibuka
    {
        perror("Gagal membuka file");
        exit(1);
    }

    char ch; // variabel yang membaca karakter
    while ((ch = fgetc(inputFile)) != EOF) // loop membaca karakter file original.txt
    {
        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == ' ' || ch == '\n') // kriteria karakter
        {
            fputc(ch, outputFile); // jika memenuhi kriteria maka karakter dimasukkan ke dalam file thebeatles.txt
        }
    }

    fclose(inputFile);
    fclose(outputFile);
}

// Fungsi untuk mencatat log
void entryLog(const char *type, const char *message) 
{
    time_t current_time; // waktu saat ini
    struct tm *time_info; // informasi waktu terstruktur seperti tanggal, bulan, tahun, dll.
    char timeString[30]; // untuk menyimpan tanggal dan waktu

    // mendapatkan waktu saat ini lalu dikonversikan ke bentuk waktu yang terstruktur
    time(&current_time);
    time_info = localtime(&current_time);

    strftime(timeString, sizeof(timeString), "[%d/%m/%y %H:%M:%S]", time_info); // memformat waktu

    // membuka file logfile
    FILE *logFile = fopen("frekuensi.log", "a");
    if (logFile == NULL) {
        perror("Gagal Membuka File Log");
        exit(1);
    }

    fprintf(logFile, "%s [%s] %s\n", timeString, type, message); // mencetak pesan ke log 
    fclose(logFile);
}

// fungsi untuk menghitung jumlah kata yg dicari 
void hitungKata() 
{
    FILE *file = fopen("thebeatles.txt", "r"); // membuka file thebeatles.txt
    if (file == NULL) 
    {
        perror("Gagal Membuka File");
        exit(1);
    }

    char kata[MAX_BUFFER_SIZE]; // variabel kata untuk menyimpan karakter yang dibaca
    int wordCount = 0; // menghitung frekuensi kemunculan kata yang dicari
    char kata_dicari[MAX_BUFFER_SIZE]; // variabel untuk menyimpan kata yang ingin dicari
    int frekuensi_kata = 0; //untuk menyimpan total frekuensi kemunculan kata yang dicari

    printf("Masukkan kata yang ingin dicari: ");
    scanf("%s", kata_dicari);

    while (fscanf(file, "%s", kata) == 1) // loop membaca kata dari berkas thebeatles.txt satu persatu
    {
        if (strcmp(kata, kata_dicari) == 0) // jika kata sama dengan kata yang dicari
        {
            frekuensi_kata++; // inkremen jumlah kata yg dicari
        }
    }
    printf("Huruf '%s' sebanyak %d kali dalam file thebeatles.txt\n", kata_dicari, frekuensi_kata);

    // Mencatat hasil perhitungan dalam log
    char logMessage[2048];
    snprintf(logMessage, sizeof(logMessage), "Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'", kata_dicari, frekuensi_kata);
    entryLog("KATA", logMessage);

    fclose(file);
}

// Fungsi untuk menghitung jumlah huruf yg dicari
void hitungHuruf() 
{
    FILE *file = fopen("thebeatles.txt", "r");
    if (file == NULL) 
    {
        perror("Gagal Membuka File");
        exit(1);
    }

    char huruf;
    char huruf_dicari;
    int huruf_frekuensi = 0;

    printf("Masukkan huruf yang ingin dicari: ");
    scanf(" %c", &huruf_dicari);

    while ((huruf = fgetc(file)) != EOF) 
    {
        if (huruf == huruf_dicari) {
            huruf_frekuensi++;
        }
    }

    printf("Huruf '%c' muncul sebanyak %d kali dalam file thebeatles.txt\n", huruf_dicari, huruf_frekuensi);

    // Mencatat hasil perhitungan dalam log
    char logMessage[1024];
    snprintf(logMessage, sizeof(logMessage), "Huruf '%c' muncul sebanyak %d kali dalam file 'thebeatles.txt'", huruf_dicari, huruf_frekuensi);
    entryLog("HURUF", logMessage);

    fclose(file);
}


int main(int argc, char *argv[]) // variabel argc untuk menjalankan program, argv untuk perintah -kata atau -huruf
{
    // memeriksa apakah perintah cocok dengan kriteria
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) 
    {
        printf("Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }

    downloadFile("https://drive.google.com/uc?id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW"); // call fungsi downloadFile
    filterFile(); // call fungsi filterFile

    if (strcmp(argv[1], "-kata") == 0) // jika memanggil perintah menghitung kata
    {
        hitungKata();
    }
    else 
    {
        hitungHuruf();
    }

    return 0;
}
