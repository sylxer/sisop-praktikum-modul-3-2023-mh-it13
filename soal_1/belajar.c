#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/shm.h>

int main() {
    srand(time(NULL));

    // Matriks pertama (3x2)
    int matriks1[3][2];
    printf("Matriks pertama:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 4 + 1;
            printf("%d ", matriks1[i][j]);
        }
        printf("] \n");
    }

    // Matriks kedua (2x3)
    int matriks2[2][3];
    printf("Matriks kedua:\n");
    for (int i = 0; i < 2; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            matriks2[i][j] = rand() % 5 + 1;
            printf("%d ", matriks2[i][j]);
        }
        printf("] \n");
    }

    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[3][3]), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    int (*hasil)[3][3];
    hasil = shmat(shmid, NULL, 0);

    // Menghitung hasil perkalian
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            int hasil_kali = 0;
            for (int k = 0; k < 2; k++) {
                hasil_kali += matriks1[i][k] * matriks2[k][j];
            }
            (*hasil)[i][j] = hasil_kali;
        }
    }

    printf("Hasil perkalian:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }

    // Mengurangi 1 dari setiap elemen hasil perkalian
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            (*hasil)[i][j] -= 1;
        }
    }

    printf("Hasil perkalian dikurangi 1:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }

    // Transpose matriks hasil perkalian
    int temp;
    for (int i = 0; i < 3; i++) {
        for (int j = i + 1; j < 3; j++) {
            temp = (*hasil)[i][j];
            (*hasil)[i][j] = (*hasil)[j][i];
            (*hasil)[j][i] = temp;
        }
    }

    printf("Matriks transpose:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }

    shmdt(hasil);
    return 0;
}
