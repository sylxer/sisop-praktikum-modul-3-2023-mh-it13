#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h>

unsigned long long factorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    return n * factorial(n - 1);
}

struct ThreadArgs {
    int row;
    int col;
};

int transposedMatrix[3][3];
unsigned long long factorialMatrix[3][3];

void* calculateFactorial(void* arg) {
    struct ThreadArgs* args = (struct ThreadArgs*)arg;
    int row = args->row;
    int col = args->col;

    factorialMatrix[row][col] = factorial(transposedMatrix[row][col]);

    pthread_exit(NULL);
}

int main() {
    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[3][3]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    int (*shmarr)[3][3];
    shmarr = shmat(shmid, NULL, 0);

    
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            transposedMatrix[i][j] = (*shmarr)[i][j];
        }
    }

    pthread_t threads[3][3];

    printf("Matriks hasil transposisi:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d\t", transposedMatrix[i][j]);
        }
        printf("\n");
    }

    clock_t start, finish;
    double cpu_time;

    start = clock(); // Record start time

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            struct ThreadArgs* args = (struct ThreadArgs*)malloc(sizeof(struct ThreadArgs));
            args->row = i;
            args->col = j;
            pthread_create(&threads[i][j], NULL, calculateFactorial, args);
        }
    }

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            pthread_join(threads[i][j], NULL);
        }
    }

    finish = clock(); // Record finish time

    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%llu\t", factorialMatrix[i][j]);
        }
        printf("\n");
    }

    // Calculate CPU time
    cpu_time = ((double)(finish - start)) / CLOCKS_PER_SEC;

    // Display CPU time
    printf("CPU time: %f seconds\n", cpu_time);

    return 0;
}
