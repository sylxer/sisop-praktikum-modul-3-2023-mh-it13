#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

// Fungsi untuk menghitung faktorial
unsigned long long factorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    return n * factorial(n - 1);
}

int main() {
    clock_t start, finish;
    double cpu_time;
    start = clock();

    // Meng-attach shared memory dari program "belajar.c"
    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[3][3]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    int (*shmarr)[3][3];
    shmarr = shmat(shmid, NULL, 0); 

    // Matriks hasil transposisi dari shared memory
    int transposedMatrix[3][3];
    
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            transposedMatrix[i][j] = (*shmarr)[i][j];
        }
    }

    // Matriks hasil faktorial
    unsigned long long factorialMatrix[3][3];

    // Menampilkan matriks hasil transposisi
    printf("Matriks hasil transposisi:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d\t", transposedMatrix[i][j]);
        }
        printf("\n");
    }

    // Menghitung faktorial secara berurutan
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            factorialMatrix[i][j] = factorial(transposedMatrix[i][j]);
        }
    }

    // Menampilkan matriks hasil faktorial
    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%llu\t", factorialMatrix[i][j]);
        }
        printf("\n");
    }

    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    printf("CPU time: %f s\n", cpu_time);

    return 0;
}
