#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define MAX_MESSAGE_LENGTH 256

int main()
{
    int client_socket;
    char client_message[MAX_MESSAGE_LENGTH];

    // Create client socket
    client_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (client_socket == -1)
    {
        perror("Error creating client socket");
        exit(1);
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(9002);
    server_address.sin_addr.s_addr = INADDR_ANY;

    // Connect to the server
    if (connect(client_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1)
    {
        perror("Error connecting to server");
        exit(1);
    }

    while (1)
    {
        printf("Enter a message: ");
        fgets(client_message, sizeof(client_message), stdin);

        // Remove trailing newline character
        client_message[strcspn(client_message, "\r\n")] = '\0';

        // Check if the user wants to exit
        if (strcmp(client_message, "exit") == 0)
        {
            break;
        }

        // Send the message to the server
        send(client_socket, client_message, strlen(client_message), 0);
    }

    // Close the socket
    close(client_socket);

    return 0;
}
