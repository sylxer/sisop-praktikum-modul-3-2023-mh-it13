#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>

#define MAX_CLIENTS 5
#define MAX_MESSAGE_LENGTH 256

pthread_mutex_t client_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t client_cond = PTHREAD_COND_INITIALIZER;
int num_clients = 0;

void *client_handler(void *arg) {
    int client_socket = *(int *)arg;
    char client_message[MAX_MESSAGE_LENGTH];

    while (1) {
        ssize_t num_bytes = recv(client_socket, client_message, sizeof(client_message) - 1, 0);
        if (num_bytes == -1) {
            perror("Error receiving data from client");
            break;
        } else if (num_bytes == 0) {
            // Connection closed by client
            break;
        }

        client_message[num_bytes] = '\0';
        printf("%s\n", client_message);

        if (strcmp(client_message, "exit") == 0) {
            break;
        }
    }

    pthread_mutex_lock(&client_mutex);
    num_clients--;
    pthread_cond_signal(&client_cond);
    pthread_mutex_unlock(&client_mutex);

    close(client_socket);
    free(arg);
    return NULL;
}

int main() {
    int server_socket, client_socket;
    int *new_client;
    pthread_t client_thread;
    char client_message[MAX_MESSAGE_LENGTH];

    server_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (server_socket == -1) {
        perror("Error creating server socket");
        exit(1);
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(9002);
    server_address.sin_addr.s_addr = INADDR_ANY;

    int reuse = 1;
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1) {
        perror("Error setting socket options");
        exit(1);
    }

    if (bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1) {
        perror("Error binding socket");
        exit(1);
    }

    if (listen(server_socket, MAX_CLIENTS) == -1) {
        perror("Error listening for connections");
        exit(1);
    }

    while (1) {
        client_socket = accept(server_socket, NULL, NULL);
        if (client_socket == -1) {
            perror("Error accepting connection");
        } else {
            pthread_mutex_lock(&client_mutex);
            
            if (num_clients >= MAX_CLIENTS) {
                pthread_mutex_unlock(&client_mutex);
                printf("Aigooo, ini sudah lebih dari 5 client loo. Phei a phei\n");
                close(client_socket);
            } else {
                num_clients++;
                pthread_mutex_unlock(&client_mutex);

                new_client = (int *)malloc(sizeof(int));
                *new_client = client_socket;

                if (pthread_create(&client_thread, NULL, client_handler, (void *)new_client) != 0) {
                    perror("Error creating client thread");
                    free(new_client);
                }
            }
        }
    }

    close(server_socket);
    return 0;
}
