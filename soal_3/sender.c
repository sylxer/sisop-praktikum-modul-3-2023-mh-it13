#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX_MESSAGE_SIZE 1024
#define KEY 1234

struct Message {
    long messageType;
    char data[MAX_MESSAGE_SIZE];
};

int main() {
    // Buat atau akses message queue
    int msgid = msgget(KEY, 0666 | IPC_CREAT);
    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    // Minta pengguna untuk memasukkan pesan
    printf("Masukkan pesan yang akan dikirim: ");
    char userMessage[MAX_MESSAGE_SIZE];
    fgets(userMessage, sizeof(userMessage), stdin);
    userMessage[strcspn(userMessage, "\n")] = '\0'; // Menghilangkan karakter newline dari input

    // Buat pesan
    struct Message message;
    message.messageType = 1;
    strcpy(message.data, userMessage);

    // Kirim pesan ke receiver
    if (msgsnd(msgid, &message, sizeof(message), 0) == -1) {
        perror("msgsnd");
        exit(1);
    }

    printf("Pesan telah dikirim\n");

    // Tutup message queue
    msgctl(msgid, IPC_RMID, NULL);

    return 0;
}
