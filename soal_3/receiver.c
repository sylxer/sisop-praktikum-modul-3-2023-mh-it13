#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <base64.h> // Sisipkan header libbase64

#define MAX_MESSAGE_SIZE 1024
#define KEY 1234

struct Message {
    long messageType;
    char data[MAX_MESSAGE_SIZE];
};

int main() {
    // Buat atau akses message queue
    int msgid = msgget(KEY, 0666 | IPC_CREAT);
    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    struct Message message;

    // Terima pesan dari sender
    if (msgrcv(msgid, &message, sizeof(message), 1, 0) == -1) {
        perror("msgrcv");
        exit(1);
    }

    printf("Pesan diterima: %s\n", message.data);

    // Proses pesan CREDS
    if (strcmp(message.data, "CREDS") == 0) {
        // Baca file "users.txt" yang ada di dalam folder yang sama dengan receiver.c untuk dibaca
        FILE *file = fopen("users.txt", "r");
        if (file == NULL) {
            perror("fopen");
            exit(1);
        }

        char buffer[MAX_MESSAGE_SIZE];
        size_t bytesRead = fread(buffer, 1, sizeof(buffer), file);
        if (bytesRead == 0) {
            perror("fread");
            exit(1);
        }

        fclose(file);

        // Dekode data base64 menggunakan libbase64
        char *decodedData = NULL;
        size_t decodedDataLen = base64_decode(buffer, bytesRead, &decodedData);

        // Tampilkan hasil dekode
        if (decodedDataLen > 0) {
            decodedData[decodedDataLen] = '\0';
            printf("Decoded data from 'users.txt': %s\n", decodedData);
            free(decodedData);
        } else {
            printf("Failed to decode data from 'users.txt'\n");
        }
    }

    // Tutup message queue
    msgctl(msgid, IPC_RMID, NULL);

    return 0;
}
