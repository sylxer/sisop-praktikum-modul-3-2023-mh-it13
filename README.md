# sisop-praktikum-modul-3-2023-MH-IT13



## Anggota
1. Sylvia Febrianti - 5027221019
2. Zulfa Hafizh Kusuma - 5027221038
3. Muhammad Rifqi Oktaviansyah - 5027221067


## Soal 1
Epul memiliki seorang adik SMA bernama Azka. Azka bersekolah di Thursina Malang. Di sekolah, Azka diperkenalkan oleh adanya kata matriks. Karena Azka baru pertama kali mendengar kata matriks, Azka meminta tolong kepada kakaknya Epul untuk belajar matriks.  Karena Epul adalah seorang kakak yang baik, Epul memiliki ide untuk membuat program yang bisa membantu adik tercintanya, tetapi dia masih bingung untuk membuatnya. Karena Epul adalah bagian dari IT05 dan praktikan sisop, sebagai warga IT05 yang baik, bantu Epul mengerjakan programnya dengan ketentuan sebagai berikut :<br>
a. Membuat program C dengan nama belajar.c, yang berisi program untuk melakukan perkalian matriks. Agar ukuran matriks bervariasi, maka ukuran matriks pertama adalah [nomor_kelompok]×2 dan matriks kedua 2×[nomor_kelompok]. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-4 (inklusif), dan rentang pada matriks kedua adalah 1-5 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar. <br>
b. Karena Epul merasa ada yang kurang, Epul meminta agar hasil dari perkalian tersebut dikurang 1 di setiap matriks.<br>
c. Karena Epul baru belajar modul 3, Epul ngide ingin meminta agar menerapkan konsep shared memory. Buatlah program C kedua dengan nama yang.c. Program ini akan mengambil variabel hasil pengurangan dari perkalian matriks dari program belajar.c (program sebelumnya). Hasil dari pengurangan perkalian matriks tersebut dilakukan transpose matriks dan diperlihatkan hasilnya. 
(Catatan: wajib menerapkan konsep shared memory)<br>
d. Setelah ditampilkan, Epul pengen adiknya belajar lebih, sehingga untuk setiap angka dari transpose matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.<br>
Contoh: <br>
matriks <br>
1	2  	3<br>
2	2	4<br>
maka:<br>
1	4	6<br>
4	4	24<br>
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)<br>
e. Epul penasaran mengapa dibuat thread dan multithreading pada program sebelumnya, dengan demikian dibuatlah program C ketiga dengan nama rajin.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada yang.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. <br>
Dokumentasikan dan sampaikan saat demo dan laporan resmi.<br>

## Penyelesaian
**belajar.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/shm.h>

int main() {
    srand(time(NULL));

    // Matriks pertama (3x2)
    int matriks1[3][2];
    printf("Matriks pertama:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 4 + 1;
            printf("%d ", matriks1[i][j]);
        }
        printf("] \n");
    }

    // Matriks kedua (2x3)
    int matriks2[2][3];
    printf("Matriks kedua:\n");
    for (int i = 0; i < 2; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            matriks2[i][j] = rand() % 5 + 1;
            printf("%d ", matriks2[i][j]);
        }
        printf("] \n");
    }

    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[3][3]), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    int (*hasil)[3][3];
    hasil = shmat(shmid, NULL, 0);

    // Menghitung hasil perkalian
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            int hasil_kali = 0;
            for (int k = 0; k < 2; k++) {
                hasil_kali += matriks1[i][k] * matriks2[k][j];
            }
            (*hasil)[i][j] = hasil_kali;
        }
    }

    printf("Hasil perkalian:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }

    // Mengurangi 1 dari setiap elemen hasil perkalian
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            (*hasil)[i][j] -= 1;
        }
    }

    printf("Hasil perkalian dikurangi 1:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }

    // Transpose matriks hasil perkalian
    int temp;
    for (int i = 0; i < 3; i++) {
        for (int j = i + 1; j < 3; j++) {
            temp = (*hasil)[i][j];
            (*hasil)[i][j] = (*hasil)[j][i];
            (*hasil)[j][i] = temp;
        }
    }

    printf("Matriks transpose:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }

    shmdt(hasil);
    return 0;
}
```

**Penjelasan code belajar.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/shm.h>
```
Pada baris-baris awal ini, program ini mengimpor beberapa header file yang diperlukan. stdio.h untuk fungsi input/output standar, stdlib.h untuk fungsi-fungsi umum seperti alokasi memori, time.h untuk menghasilkan seed waktu, dan sys/shm.h untuk menggunakan shared memory.
```
    srand(time(NULL));
```
Fungsi srand(time(NULL)) digunakan untuk menginisialisasi generator angka acak dengan seed waktu saat program dijalankan. Ini akan memastikan bahwa angka acak yang dihasilkan akan berbeda setiap kali program dijalankan.
```
    int matriks1[3][2];
    printf("Matriks pertama:\n");
```
Deklarasi matriks matriks1 dengan ukuran 3x2 untuk matriks pertama. Program mencetak pesan ke layar untuk memberi tahu pengguna bahwa ini adalah matriks pertama.
```
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 4 + 1;
            printf("%d ", matriks1[i][j]);
        }
        printf("] \n");
    }
```
Program menggunakan perulangan for bersarang untuk mengisi matriks pertama matriks1 dengan angka-angka acak antara 1 hingga 4 dan mencetak matriks pertama ke layar.
```
    int matriks2[2][3];
    printf("Matriks kedua:\n");
```
Sama seperti sebelumnya, program mendefinisikan matriks kedua matriks2 dengan ukuran 2x3 dan mencetak pesan ke layar untuk memberi tahu pengguna bahwa ini adalah matriks kedua.
```
    for (int i = 0; i < 2; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            matriks2[i][j] = rand() % 5 + 1;
            printf("%d ", matriks2[i][j]);
        }
        printf("] \n");
    }
```
Program menggunakan perulangan for bersarang lagi untuk mengisi matriks kedua matriks2 dengan angka-angka acak antara 1 hingga 5 dan mencetak matriks kedua ke layar.
```
    int shmid;
    key_t key = 1234;
```
Variabel shmid digunakan untuk menyimpan hasil alokasi shared memory, dan key adalah kunci yang digunakan untuk mengidentifikasi shared memory. Kunci ini digunakan untuk mengakses segmen memori bersama.
```
    if ((shmid = shmget(key, sizeof(int[3][3]), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
```
Program menggunakan fungsi shmget untuk mencoba mengalokasikan segmen memori bersama dengan ukuran sizeof(int[3][3]). Jika operasi ini gagal, program akan menampilkan pesan kesalahan menggunakan perror dan kemudian keluar dari program dengan status keluaran 1.
```
    int (*hasil)[3][3];
    hasil = shmat(shmid, NULL, 0);
```
Variabel hasil adalah pointer ke matriks 3x3 di dalam shared memory. Program menggunakan shmat untuk mengaitkan segmen memori bersama dengan variabel hasil. Variabel ini sekarang mengacu pada memori bersama yang dapat digunakan untuk menyimpan hasil perkalian matriks.
```
    // Menghitung hasil perkalian
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            int hasil_kali = 0;
            for (int k = 0; k < 2; k++) {
                hasil_kali += matriks1[i][k] * matriks2[k][j];
            }
            (*hasil)[i][j] = hasil_kali;
        }
    }
```
Program menghitung hasil perkalian matriks matriks1 dan matriks2, dan hasilnya disimpan dalam matriks hasil yang ada di shared memory.
```
    printf("Hasil perkalian:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }
```
Program mencetak hasil perkalian matriks ke layar.
```
    // Mengurangi 1 dari setiap elemen hasil perkalian
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            (*hasil)[i][j] -= 1;
        }
    }
```
Program mengurangkan 1 dari setiap elemen dalam matriks hasil.
```
    printf("Hasil perkalian dikurangi 1:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }
```
Program mencetak matriks hasil yang sudah dikurangi 1 ke layar.
```
    // Transpose matriks hasil perkalian
    int temp;
    for (int i = 0; i < 3; i++) {
        for (int j = i + 1; j < 3; j++) {
            temp = (*hasil)[i][j];
            (*hasil)[i][j] = (*hasil)[j][i];
            (*hasil)[j][i] = temp;
        }
    }
```
Program melakukan operasi transposisi pada matriks hasil, yaitu menukar elemen-elemen matriks antara baris dan kolom yang sesuai.
```
    printf("Matriks transpose:\n");
    for (int i = 0; i < 3; i++) {
        printf(" [ ");
        for (int j = 0; j < 3; j++) {
            printf("%d ", (*hasil)[i][j]);
        }
        printf("] \n");
    }
```
Terakhir, program mencetak matriks hasil yang sudah ditransposisi, yang sekarang merupakan matriks transpose dari hasil perkalian.
```
    shmdt(hasil);
    return 0;
}
```
Fungsi shmdt(hasil) digunakan untuk melepaskan segmen memori bersama yang sudah tidak digunakan lagi.


**yang.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h>

unsigned long long factorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    return n * factorial(n - 1);
}

struct ThreadArgs {
    int row;
    int col;
};

int transposedMatrix[3][3];
unsigned long long factorialMatrix[3][3];

void* calculateFactorial(void* arg) {
    struct ThreadArgs* args = (struct ThreadArgs*)arg;
    int row = args->row;
    int col = args->col;

    factorialMatrix[row][col] = factorial(transposedMatrix[row][col]);

    pthread_exit(NULL);
}

int main() {
    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[3][3]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    int (*shmarr)[3][3];
    shmarr = shmat(shmid, NULL, 0);

    
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            transposedMatrix[i][j] = (*shmarr)[i][j];
        }
    }

    pthread_t threads[3][3];

    printf("Matriks hasil transposisi:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d\t", transposedMatrix[i][j]);
        }
        printf("\n");
    }

    clock_t start, finish;
    double cpu_time;

    start = clock(); // Record start time

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            struct ThreadArgs* args = (struct ThreadArgs*)malloc(sizeof(struct ThreadArgs));
            args->row = i;
            args->col = j;
            pthread_create(&threads[i][j], NULL, calculateFactorial, args);
        }
    }

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            pthread_join(threads[i][j], NULL);
        }
    }

    finish = clock(); // Record finish time

    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%llu\t", factorialMatrix[i][j]);
        }
        printf("\n");
    }

    // Calculate CPU time
    cpu_time = ((double)(finish - start)) / CLOCKS_PER_SEC;

    // Display CPU time
    printf("CPU time: %f seconds\n", cpu_time);

    return 0;
}
```

**Penjelasan code yang.c**
Program yang.c adalah program yang menghitung faktorial dari elemen-elemen matriks yang telah di-transpose menggunakan pemrograman bersama dengan bantuan pthread (library untuk multithreading) dan shared memory.
```
unsigned long long factorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    return n * factorial(n - 1);
}
```
Fungsi factorial(int n) adalah fungsi rekursif yang menghitung faktorial dari sebuah angka n. Jika n adalah 0 atau 1, maka faktorialnya adalah 1. Jika tidak, fungsi akan memanggil dirinya sendiri untuk menghitung faktorial dari n - 1 dan mengalikannya dengan n.
```
struct ThreadArgs {
    int row;
    int col;
};
```
Dalam program ini, sebuah struktur ThreadArgs didefinisikan untuk menyimpan argumen yang akan digunakan oleh setiap thread. Struktur ini memiliki dua anggota, yaitu row dan col, yang digunakan untuk menyimpan baris dan kolom matriks.
```
int transposedMatrix[3][3];
unsigned long long factorialMatrix[3][3];
```
Program mendefinisikan dua matriks. transposedMatrix akan digunakan untuk menyimpan matriks hasil transposisi yang dibaca dari shared memory, dan factorialMatrix akan digunakan untuk menyimpan hasil faktorial dari matriks transposisi.
```
void* calculateFactorial(void* arg) {
    struct ThreadArgs* args = (struct ThreadArgs*)arg;
    int row = args->row;
    int col = args->col;

    factorialMatrix[row][col] = factorial(transposedMatrix[row][col]);

    pthread_exit(NULL);
}
```
Ini adalah fungsi yang akan dijalankan oleh setiap thread. Fungsi ini menerima argumen dalam bentuk struktur ThreadArgs, kemudian menghitung faktorial dari elemen matriks transposedMatrix pada posisi row dan col, dan hasilnya disimpan dalam factorialMatrix.
```
int main() {
    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[3][3]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    int (*shmarr)[3][3];
    shmarr = shmat(shmid, NULL, 0);
```
Dalam main(), program mengalokasikan segmen memori bersama (shared memory) menggunakan shmget. Jika alokasi gagal, program akan menampilkan pesan kesalahan dan keluar. Selanjutnya, program menggunakan shmat untuk mengaitkan segmen memori bersama ke dalam variabel shmarr.
```
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            transposedMatrix[i][j] = (*shmarr)[i][j];
        }
    }
```
Program mengambil matriks hasil transposisi dari shared memory dan menyimpannya dalam matriks transposedMatrix.
```
    pthread_t threads[3][3];

    printf("Matriks hasil transposisi:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d\t", transposedMatrix[i][j]);
        }
        printf("\n");
    }
```
Program mencetak matriks hasil transposisi ke layar.
```
    clock_t start, finish;
    double cpu_time;

    start = clock(); // Record start time
```
Program menginisialisasi pengukuran waktu menggunakan clock() sebelum menjalankan perhitungan faktorial dalam thread.
```
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            struct ThreadArgs* args = (struct ThreadArgs*)malloc(sizeof(struct ThreadArgs));
            args->row = i;
            args->col = j;
            pthread_create(&threads[i][j], NULL, calculateFactorial, args);
        }
    }
```
Program menggunakan perulangan bersarang untuk membuat dan menjalankan thread-thread yang akan menghitung faktorial untuk setiap elemen matriks.
```
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            pthread_join(threads[i][j], NULL);
        }
    }
```
Program menunggu semua thread selesai sebelum melanjutkan.
```
    finish = clock(); // Record finish time

    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%llu\t", factorialMatrix[i][j]);
        }
        printf("\n");
    }
```
Program mencetak matriks hasil faktorial ke layar.
```
    // Calculate CPU time
    cpu_time = ((double)(finish - start)) / CLOCKS_PER_SEC;

    // Display CPU time
    printf("CPU time: %f seconds\n", cpu_time);

    return 0;
}
```
Program menghitung waktu CPU yang diperlukan untuk menjalankan semua perhitungan faktorial dan mencetak waktu tersebut ke layar sebelum program selesai.


**rajin.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

// Fungsi untuk menghitung faktorial
unsigned long long factorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    return n * factorial(n - 1);
}

int main() {
    clock_t start, finish;
    double cpu_time;
    start = clock();

    // Meng-attach shared memory dari program "belajar.c"
    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[3][3]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    int (*shmarr)[3][3];
    shmarr = shmat(shmid, NULL, 0); 

    // Matriks hasil transposisi dari shared memory
    int transposedMatrix[3][3];
    
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            transposedMatrix[i][j] = (*shmarr)[i][j];
        }
    }

    // Matriks hasil faktorial
    unsigned long long factorialMatrix[3][3];

    // Menampilkan matriks hasil transposisi
    printf("Matriks hasil transposisi:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d\t", transposedMatrix[i][j]);
        }
        printf("\n");
    }

    // Menghitung faktorial secara berurutan
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            factorialMatrix[i][j] = factorial(transposedMatrix[i][j]);
        }
    }

    // Menampilkan matriks hasil faktorial
    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%llu\t", factorialMatrix[i][j]);
        }
        printf("\n");
    }

    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    printf("CPU time: %f s\n", cpu_time);

    return 0;
}
```

**Penjelasan code rajin.c**
Program rajin.c mengambil matriks hasil transposisi dari shared memory yang dihasilkan oleh program "belajar.c" dan menghitung faktorial dari setiap elemennya. Selanjutnya, program ini juga mengukur waktu CPU yang diperlukan untuk melakukan perhitungan faktorial. 
```
// Fungsi untuk menghitung faktorial
unsigned long long factorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    return n * factorial(n - 1);
}
```
Program mendefinisikan fungsi factorial(int n) yang digunakan untuk menghitung faktorial dari suatu bilangan bulat n. Jika n adalah 0 atau 1, maka fungsi ini mengembalikan 1. Jika tidak, fungsi ini memanggil dirinya sendiri untuk menghitung faktorial dari n - 1 dan mengalikan hasilnya dengan n.
```
    // Meng-attach shared memory dari program "belajar.c"
    int shmid;
    key_t key = 1234;

    if ((shmid = shmget(key, sizeof(int[3][3]), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    int (*shmarr)[3][3];
    shmarr = shmat(shmid, NULL, 0); 
```
Program menggunakan shared memory untuk mengambil data matriks hasil transposisi dari program "belajar.c". Ini melibatkan operasi seperti alokasi shared memory dengan shmget, mengaitkan shared memory dengan shmat, dan mengambil alamat matriks hasil transposisi dari shared memory.
```
    // Matriks hasil transposisi dari shared memory
    int transposedMatrix[3][3];
    
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            transposedMatrix[i][j] = (*shmarr)[i][j];
        }
    }
```
Program mengambil matriks hasil transposisi dari shared memory dan menyimpannya dalam matriks transposedMatrix.
```
    // Menghitung faktorial secara berurutan
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            factorialMatrix[i][j] = factorial(transposedMatrix[i][j]);
        }
    }
```
Program menghitung faktorial dari setiap elemen matriks hasil transposisi dan menyimpan hasilnya dalam matriks factorialMatrix.
```
    // Menampilkan matriks hasil faktorial
    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%llu\t", factorialMatrix[i][j]);
        }
        printf("\n");
    }
```
Program mencetak matriks hasil faktorial ke layar.
```
    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    printf("CPU time: %f s\n", cpu_time);

    return 0;
}
```
Program mengukur waktu CPU yang diperlukan untuk menjalankan operasi dan mencetak waktu tersebut ke layar sebelum program selesai.


## Screenshot
- Output belajar.c, yang.c, dan rajin.c
![Output](https://gitlab.com/sylxer/sisop-praktikum-modul-3-2023-mh-it13/-/raw/main/Screenshot/soal_1/output.png)


## Kesimpulan
Pada percobaan dalam kode yang.c dan rajin.c dapat disimpulkan bahwa kode yang menggunakan thread lebih lama dalam hal waktu eksekusi karena terdapat overhead yang diperlukan untuk menginisialisasi thread dan memindahkan data di antara thread yang berbeda.<br>
Dalam kasus kode ini, menggunakan thread untuk menghitung faktorial masing-masing baris matriks mengharuskan program untuk membuat thread baru untuk setiap baris. Proses ini memakan waktu karena harus mengalokasikan memori dan membuat struktur data untuk setiap thread, serta memindahkan data yang diperlukan ke dalam thread.<br>
Sementara pada kode tanpa menggunakan thread, perhitungan faktorial dilakukan secara berurutan pada setiap baris matriks tanpa overhead tambahan dari thread, sehingga waktu eksekusinya lebih cepat.<br>
Namun, penggunaan thread dapat menjadi lebih efisien dalam situasi di mana ada banyak tugas yang dapat dijalankan secara bersamaan dan membutuhkan waktu yang lama untuk menyelesaikan tugas tersebut. Dalam kasus seperti itu, menggunakan thread dapat memungkinkan program untuk mengeksekusi tugas-tugas tersebut secara bersamaan dan mengurangi waktu keseluruhan yang dibutuhkan untuk menyelesaikan semua tugas.<br>


## Soal 2

Messi the only GOAT sedang gabut karena Inter Miami Gagal masuk Playoff. Messi menghabiskan waktu dengan mendengar lagu. Karena gabut yang tergabut gabut, Messi penasaran berapa banyak jumlah kata tertentu yang muncul pada lagu tersebut. Messi mencoba ngoding untuk mencari jawaban dari kegabutannya. Pertama-tama dia mengumpulkan beberapa lirik lagu favorit yang disimpan di https://drive.google.com/file/d/16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW/view?usp=share_link. Beberapa saat setelah mencoba ternyata ngoding tidak semudah itu, Messipun meminta tolong kepada Ronaldo, tetapi karena Ronaldo sibuk main di liga oli, Ronaldopun meminta tolong kepadamu untuk dibuatkan kode dari program Messi dengan ketentuan sebagai berikut : 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).
- Messi ingin nama programnya 8ballondors.c. Pada parent process, program akan membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan membuat output file dari hasil proses itu dengan nama thebeatles.txt
- Pada child process, lakukan perhitungan jumlah frekuensi kemunculan sebuah kata dari output file yang akan diinputkan (kata) oleh user.
- Karena gabutnya, Messi ingin program dapat mencari jumlah frekuensi kemunculan sebuah huruf dari file. Maka, pada child process lakukan perhitungan jumlah frekuensi kemunculan sebuah huruf dari output file yang akan diinputkan (huruf) user.
- Karena terdapat dua perhitungan, maka pada program buatlah argumen untuk menjalankan program : 
Kata	: ./8ballondors -kata
Huruf	: ./8ballondors -huruf
- Hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process.
- Messi ingin agar setiap kata atau huruf dicatat dalam sebuah log yang diberi nama frekuensi.log. Pada parent process, lakukan pembuatan file log berdasarkan data yang dikirim dari child process. 
    Format: [date] [type] [message]
    Type: KATA, HURUF
    Ex:
    1. [24/10/23 01:05:48] [KATA] Kata 'yang' muncul sebanyak 10 kali dalam file 'thebeatles.txt'
    2. [24/10/23 01:04:29] [HURUF] Huruf 'a' muncul sebanyak 396 kali dalam file 'thebeatles.txt'
Catatan:
Perhitungan jumlah frekuensi kemunculan kata atau huruf  menggunakan Case-Sensitive Search. 

## Penyelesaian

**8ballondors.c**

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

#define MAX_BUFFER_SIZE 1024

// Fungsi download file
void downloadFile(const char *url) 
{
    system("wget -O original.txt https://drive.google.com/uc?id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW");
}

// Fungsi untuk memfilter file dari karakter non huruf dari original.txt ke dalam thebeatles.txt
void filterFile() 
{
    FILE *inputFile = fopen("original.txt", "r"); // membuka file original.txt pada mode baca "r"
    FILE *outputFile = fopen("thebeatles.txt", "w"); // membuka file original.txt pada mode tulis "w"

    if (inputFile == NULL || outputFile == NULL) // mengecek apakah file bisa dibuka
    {
        perror("Gagal membuka file");
        exit(1);
    }

    char ch; // variabel yang membaca karakter
    while ((ch = fgetc(inputFile)) != EOF) // loop membaca karakter file original.txt
    {
        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == ' ' || ch == '\n') // kriteria karakter
        {
            fputc(ch, outputFile); // jika memenuhi kriteria maka karakter dimasukkan ke dalam file thebeatles.txt
        }
    }

    fclose(inputFile);
    fclose(outputFile);
}

// Fungsi untuk mencatat log
void entryLog(const char *type, const char *message) 
{
    time_t current_time; // waktu saat ini
    struct tm *time_info; // informasi waktu terstruktur seperti tanggal, bulan, tahun, dll.
    char timeString[30]; // untuk menyimpan tanggal dan waktu

    // mendapatkan waktu saat ini lalu dikonversikan ke bentuk waktu yang terstruktur
    time(&current_time);
    time_info = localtime(&current_time);

    strftime(timeString, sizeof(timeString), "[%d/%m/%y %H:%M:%S]", time_info); // memformat waktu

    // membuka file logfile
    FILE *logFile = fopen("frekuensi.log", "a");
    if (logFile == NULL) {
        perror("Gagal Membuka File Log");
        exit(1);
    }

    fprintf(logFile, "%s [%s] %s\n", timeString, type, message); // mencetak pesan ke log 
    fclose(logFile);
}

// fungsi untuk menghitung jumlah kata yg dicari 
void hitungKata() 
{
    FILE *file = fopen("thebeatles.txt", "r"); // membuka file thebeatles.txt
    if (file == NULL) 
    {
        perror("Gagal Membuka File");
        exit(1);
    }

    char kata[MAX_BUFFER_SIZE]; // variabel kata untuk menyimpan karakter yang dibaca
    int wordCount = 0; // menghitung frekuensi kemunculan kata yang dicari
    char kata_dicari[MAX_BUFFER_SIZE]; // variabel untuk menyimpan kata yang ingin dicari
    int frekuensi_kata = 0; //untuk menyimpan total frekuensi kemunculan kata yang dicari

    printf("Masukkan kata yang ingin dicari: ");
    scanf("%s", kata_dicari);

    while (fscanf(file, "%s", kata) == 1) // loop membaca kata dari berkas thebeatles.txt satu persatu
    {
        if (strcmp(kata, kata_dicari) == 0) // jika kata sama dengan kata yang dicari
        {
            frekuensi_kata++; // inkremen jumlah kata yg dicari
        }
    }
    printf("Huruf '%s' sebanyak %d kali dalam file thebeatles.txt\n", kata_dicari, frekuensi_kata);

    // Mencatat hasil perhitungan dalam log
    char logMessage[2048];
    snprintf(logMessage, sizeof(logMessage), "Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'", kata_dicari, frekuensi_kata);
    entryLog("KATA", logMessage);

    fclose(file);
}

// Fungsi untuk menghitung jumlah huruf yg dicari
void hitungHuruf() 
{
    FILE *file = fopen("thebeatles.txt", "r");
    if (file == NULL) 
    {
        perror("Gagal Membuka File");
        exit(1);
    }

    char huruf;
    char huruf_dicari;
    int huruf_frekuensi = 0;

    printf("Masukkan huruf yang ingin dicari: ");
    scanf(" %c", &huruf_dicari);

    while ((huruf = fgetc(file)) != EOF) 
    {
        if (huruf == huruf_dicari) {
            huruf_frekuensi++;
        }
    }

    printf("Huruf '%c' muncul sebanyak %d kali dalam file thebeatles.txt\n", huruf_dicari, huruf_frekuensi);

    // Mencatat hasil perhitungan dalam log
    char logMessage[1024];
    snprintf(logMessage, sizeof(logMessage), "Huruf '%c' muncul sebanyak %d kali dalam file 'thebeatles.txt'", huruf_dicari, huruf_frekuensi);
    entryLog("HURUF", logMessage);

    fclose(file);
}


int main(int argc, char *argv[]) // variabel argc untuk menjalankan program, argv untuk perintah -kata atau -huruf
{
    // memeriksa apakah perintah cocok dengan kriteria
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) 
    {
        printf("Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }

    downloadFile("https://drive.google.com/uc?id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW"); // call fungsi downloadFile
    filterFile(); // call fungsi filterFile

    ifk (strcmp(argv[1], "-ata") == 0) // jika memanggil perintah menghitung kata
    {
        hitungKata();
    }
    else 
    {
        hitungHuruf();
    }

    return 0;
}

```

## Penjelasan Code

### Langkah pertama adalah membuat file 8ballondors.c. (Saya menggunakan terminal wsl yang tersambung dengan vscode)
```
code 8ballondors.c
```
### Langkah selanjutnya adalah membuat fungsi untuk mendownload lirik lagu dari link
```
void downloadFile(const char *url) 
{
    system("wget -O original.txt https://drive.google.com/uc?id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW");
}
```
Pada fungsi menggunakan system() untuk mendownload lirik dari link drive yang sudah disediakan dan menyimpannya di file original.txt.

### Selanjutnya, membuat fungsi untuk memfilter karakter huruf lalu ditempatkan di file thebeatles.txt
```
void filterFile() 
{
    FILE *inputFile = fopen("original.txt", "r"); // membuka file original.txt pada mode baca "r"
    FILE *outputFile = fopen("thebeatles.txt", "w"); // membuka file thebeatles.txt pada mode tulis "w"

    if (inputFile == NULL || outputFile == NULL) // mengecek apakah file bisa dibuka
    {
        perror("Gagal membuka file");
        exit(1);
    }

    char ch; // variabel yang membaca karakter
    while ((ch = fgetc(inputFile)) != EOF) // loop membaca karakter file original.txt
    {
        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == ' ' || ch == '\n') // kriteria karakter
        {
            fputc(ch, outputFile); // jika memenuhi kriteria maka karakter dimasukkan ke dalam file thebeatles.txt
        }
    }

    fclose(inputFile);
    fclose(outputFile);
}
```
Dua baris kode dibawah berfungsi untuk membuka file original.txt dan thebeatles.txt
```
    FILE *inputFile = fopen("original.txt", "r"); // membuka file original.txt pada mode baca "r"
    FILE *outputFile = fopen("thebeatles.txt", "w"); // membuka file original.txt pada mode tulis "w"
```
Lalu, kode if dibawah berfungsi untuk mengecek apakah file bisa dibuka
```
if (inputFile == NULL || outputFile == NULL) // mengecek apakah file bisa dibuka
    {
        perror("Gagal membuka file");
        exit(1);
    }
```
Setelah itu, pada kode dibawah di deklarasikan variabel bernama ch yang berguna untuk membaca karakter dalam file
```
char ch; 
```
Lalu, kode dibawah merupakan loop while yang bertujuan untuk membaca karakter satu-persatu. Didalam loop terdapat fungsi if yang mempunyai syarat karakter-karakter yang akan difilter. Karakter-karakternya diantara lain adalah huruf kecil, huruf kapital, spasi, dan enter. Jika karakter memenuhi syarat maka karakter akan dimasukkan ke file thebeatles.txt
```
while ((ch = fgetc(inputFile)) != EOF) // loop membaca karakter file original.txt
    {
        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == ' ' || ch == '\n') // kriteria karakter
        {
            fputc(ch, outputFile); // jika memenuhi kriteria maka karakter dimasukkan ke dalam file thebeatles.txt
        }
    }
```
Setelah itu, dua file tersebut ditutup dengan kode dibawah
```
    fclose(inputFile);
    fclose(outputFile);
```
### Langkah keempat adalah membuat fungsi untuk mencatat log
```
void entryLog(const char *type, const char *message) 
{
    time_t current_time; // waktu saat ini
    struct tm *time_info; // informasi waktu terstruktur seperti tanggal, bulan, tahun, dll.
    char timeString[30]; // untuk menyimpan tanggal dan waktu

    // mendapatkan waktu saat ini lalu dikonversikan ke bentuk waktu yang terstruktur
    time(&current_time);
    time_info = localtime(&current_time);

    strftime(timeString, sizeof(timeString), "[%d/%m/%y %H:%M:%S]", time_info); // memformat waktu

    // membuka file logfile
    FILE *logFile = fopen("frekuensi.log", "a");
    if (logFile == NULL) {
        perror("Gagal Membuka File Log");
        exit(1);
    }

    fprintf(logFile, "%s [%s] %s\n", timeString, type, message); // mencetak pesan ke log 
    fclose(logFile);
}
```
Pada kode dibawah dideklarasikan tiga variabel yaitu variabel current_time untuk menunjukkan waktu saat ini, variabel time_info untuk informasi waktu yang terstruktur seperti yang disuruh pada soal, dan variabel timeString untuk menyimpan waktu
```
    time_t current_time; // waktu saat ini
    struct tm *time_info; // informasi waktu terstruktur seperti tanggal, bulan, tahun, dll.
    char timeString[30]; // untuk menyimpan tanggal dan waktu
```
Dua baris kode dibawah berfungsi untuk mendapatkan waktu saat ini lalu dikonversikan ke dalam bentuk waktu yang terstruktur
```
    time(&current_time);
    time_info = localtime(&current_time);
```
Lalu, baris kode dibawah digunakan untuk memformat waktu seperti yang diminta pada soal
```
strftime(timeString, sizeof(timeString), "[%d/%m/%y %H:%M:%S]", time_info); // memformat waktu
```
Kode dibawah berguna untuk membuka log file frekuensi.log dan menampilkan pesan jika gagal membuka file
```
 FILE *logFile = fopen("frekuensi.log", "a");
 if (logFile == NULL) {
        perror("Gagal Membuka File Log");
        exit(1);
    }
```
Dua baris kode dibawah berguna untuk mencatat pesan ke dalam log lalu menutup file log
```
    fprintf(logFile, "%s [%s] %s\n", timeString, type, message); // mencetak pesan ke log 
    fclose(logFile);
```
### Tahap selanjutnya adalah membuat fungsi untuk menghitung jumlah kata yang dicari
```
void hitungKata() 
{
    FILE *file = fopen("thebeatles.txt", "r"); // membuka file thebeatles.txt
    if (file == NULL) 
    {
        perror("Gagal Membuka File");
        exit(1);
    }

    char kata[MAX_BUFFER_SIZE]; // variabel kata untuk menyimpan karakter yang dibaca
    int wordCount = 0; // menghitung frekuensi kemunculan kata yang dicari
    char kata_dicari[MAX_BUFFER_SIZE]; // variabel untuk menyimpan kata yang ingin dicari
    int frekuensi_kata = 0; //untuk menyimpan total frekuensi kemunculan kata yang dicari

    printf("Masukkan kata yang ingin dicari: ");
    scanf("%s", kata_dicari);

    while (fscanf(file, "%s", kata) == 1) // loop membaca kata dari berkas thebeatles.txt satu persatu
    {
        if (strcmp(kata, kata_dicari) == 0) // jika kata sama dengan kata yang dicari
        {
            frekuensi_kata++; // inkremen jumlah kata yg dicari
        }
    }
    printf("Huruf '%s' sebanyak %d kali dalam file thebeatles.txt\n", kata_dicari, frekuensi_kata);

    // Mencatat hasil perhitungan dalam log
    char logMessage[2048];
    snprintf(logMessage, sizeof(logMessage), "Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'", kata_dicari,    frekuensi_kata);
    entryLog("KATA", logMessage);

    fclose(file);
}
```
Kode dibawah berguna untuk membuka file thebeatles.txt dan menampilkan pesan jika gagal membuka file
```
FILE *file = fopen("thebeatles.txt", "r"); // membuka file thebeatles.txt
if (file == NULL) 
    {
        perror("Gagal Membuka File");
        exit(1);
    }
```
Lalu, kode berguna dibawah berfungsi untuk membuat 4 variabel dimana variabel kata berguna untuk menyimpan karakter yang dibaca, variabel wordCount untuk menghitung frekuensi kemunculan kata yg dicari, variabel kata_dicari untuk menyimpan kata yang ingin dicari, dan variabel frekuensi_kata untuk menyimpan total frekuensi kemunculan kata yang dicari
```
    char kata[MAX_BUFFER_SIZE]; // variabel kata untuk menyimpan karakter yang dibaca
    int wordCount = 0; // menghitung frekuensi kemunculan kata yang dicari
    char kata_dicari[MAX_BUFFER_SIZE]; // variabel untuk menyimpan kata yang ingin dicari
    int frekuensi_kata = 0; //untuk menyimpan total frekuensi kemunculan kata yang dicari
```
Kode dibawah untuk menerima input kata yang ingin kita cari 
```
    printf("Masukkan kata yang ingin dicari: ");
    scanf("%s", kata_dicari);
```
Kode while loop dibawah berguna untuk membaca kata dari file thebeatles.txt satu persatu lalu, jika ditemukan kata yg kita input maka variabel frekuensi_kata di inkremen
```
while (fscanf(file, "%s", kata) == 1) // loop membaca kata dari berkas thebeatles.txt satu persatu
    {
    if (strcmp(kata, kata_dicari) == 0) // jika kata sama dengan kata yang dicari
        {
            frekuensi_kata++; // inkremen jumlah kata yg dicari
        }
    }
```
Lalu, kode dibawah berguna untuk menampilkan pesan pada terminal
```
printf("Huruf '%s' sebanyak %d kali dalam file thebeatles.txt\n", kata_dicari, frekuensi_kata);
```
Kode dibawah berfungsi untuk mencatat pesan ke dalam file log frekuensi.log dengan cara memanggil fungsi entryLog
```
char logMessage[2048];
snprintf(logMessage, sizeof(logMessage), "Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'", kata_dicari, frekuensi_kata);
entryLog("KATA", logMessage);
```
Lalu, kode dibawah untuk menutup file thebeatles.txt
```
fclose(file);
```
### Selanjutnya, adalah membuat file untuk menghitung jumlah huruf yang dicari
```
void hitungHuruf() 
{
    FILE *file = fopen("thebeatles.txt", "r");
    if (file == NULL) 
    {
        perror("Gagal Membuka File");
        exit(1);
    }

    char huruf;
    char huruf_dicari;
    int huruf_frekuensi = 0;

    printf("Masukkan huruf yang ingin dicari: ");
    scanf(" %c", &huruf_dicari);

    while ((huruf = fgetc(file)) != EOF) 
    {
        if (huruf == huruf_dicari) {
            huruf_frekuensi++;
        }
    }

    printf("Huruf '%c' muncul sebanyak %d kali dalam file thebeatles.txt\n", huruf_dicari, huruf_frekuensi);

    // Mencatat hasil perhitungan dalam log
    char logMessage[1024];
    snprintf(logMessage, sizeof(logMessage), "Huruf '%c' muncul sebanyak %d kali dalam file 'thebeatles.txt'", huruf_dicari, huruf_frekuensi);
    entryLog("HURUF", logMessage);

    fclose(file);
}
```
Secara struktur, fungsi hitungHuruf ini sama dengan fungsi hitungKata. Dimana program akan membuka file thebeatles.txt lalu membuat variabel-variabel yang diperlukan, setelah itu program menerima input huruf. Lalu, program akan menggunakan while loop untuk mencari huruf yang kita input pada file. Jika huruf ditemukan maka variabel frekuensi huruf akan di inkremen. Setelah itu, program akan menampilkan pesan dan mencatat pesan didalam log

### Langkah terakhir adalah memanggil fungsi pada fungsi main
```
int main(int argc, char *argv[]) // variabel argc untuk menjalankan program, argv untuk perintah -kata atau -huruf
{
    // memeriksa apakah perintah cocok dengan kriteria
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) 
    {
        printf("Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }

    downloadFile("https://drive.google.com/uc?id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW"); // call fungsi downloadFile
    filterFile(); // call fungsi filterFile

    ifk (strcmp(argv[1], "-ata") == 0) // jika memanggil perintah menghitung kata
    {
        hitungKata();
    }
    else 
    {
        hitungHuruf();
    }

    return 0;
}
```
Baris kode dibawah berfungsi untuk mengecek apakah perintah yang kita masukkan benar yaitu -kata atau -huruf
```
if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) 
    {
        printf("Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }
```
Lalu, kode dibawah untuk memanggil fungsi download dengan parameter link nya dan fungsi filter file
```
    downloadFile("https://drive.google.com/uc?id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW"); // call fungsi downloadFile
    filterFile(); // call fungsi filterFile
```
Fungsi dibawah merupakan fungsi if untuk memeriksa perintah kita, jika kita memasukkan perintah -kata maka akan memanggil fungsi hitungKata. Jika tidak, maka akan memanggil fungsi hitungHuruf
```
if (strcmp(argv[1], "-kata") == 0) // jika memanggil perintah menghitung kata
    {
        hitungKata();
    }
    else 
    {
        hitungHuruf();
    }
```

## Screenshot
- Run program mencari kata
![Cari Kata](https://gitlab.com/sylxer/sisop-praktikum-modul-3-2023-mh-it13/-/raw/main/Screenshot/soal_2/-kata.png)

- Run program mencari huruf
![Cari Huruf](https://gitlab.com/sylxer/sisop-praktikum-modul-3-2023-mh-it13/-/raw/main/Screenshot/soal_2/-huruf.png)

- Tampilan file asli / original.txt
![File Original](https://gitlab.com/sylxer/sisop-praktikum-modul-3-2023-mh-it13/-/raw/main/Screenshot/soal_2/originaltxt.png)

- Tampilan file yang sudah di filter / thebeatles.txt
![File Filter](https://gitlab.com/sylxer/sisop-praktikum-modul-3-2023-mh-it13/-/raw/main/Screenshot/soal_2/thebeatles.png)

- Tampilan Log
![Log file](https://gitlab.com/sylxer/sisop-praktikum-modul-3-2023-mh-it13/-/raw/main/Screenshot/soal_2/log.png)

## Soal 3
Christopher adalah seorang praktikan sisop, dia mendapat tugas dari pak Nolan untuk membuat komunikasi antar proses dengan menerapkan konsep message queue. Pak Nolan memberikan kredensial list  users yang harus masuk ke dalam program yang akan dibuat. Lebih lanjutnya pak Nolan memberikan instruksi tambahan sebagai berikut : 
Bantulah Christopher untuk membuat program tersebut, dengan menerapkan konsep message queue(wajib) maka buatlah 2  program, sender.c sebagai pengirim dan receiver.c sebagai penerima. Dalam hal ini, sender hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh receiver.
Struktur foldernya akan menjadi seperti berikut

```
soal 3
-users
-receiver.c
-sender.c

```


Ternyata list kredensial yang diberikan Pak Nolan semua passwordnya terenkripsi dengan menggunakan base64. Untuk mengetahui kredensial yang valid maka Sender pertama kali akan mengirimkan perintah CREDS kemudian sistem receiver akan melakukan decrypt/decode/konversi pada file users, lalu menampilkannya pada receiver. Hasilnya akan menjadi seperti berikut : 

```
./receiver
Username: Mayuri, Password: TuTuRuuu
Username: Onodera, Password: K0sak!
Username: Johan, Password: L!3b3rt
Username: Seki, Password: Yuk!n3
Username: Ayanokouji, Password: K!yot4kA
```


Setelah mengetahui list kredensial, bantulah christopher untuk membuat proses autentikasi berdasarkan list kredensial yang telah disediakan. Proses autentikasi dilakukan dengan menggunakan perintah AUTH: username password kemudian jika proses autentikasi valid dan berhasil maka akan menampilkan Authentication successful . Authentication failed jika gagal.
Setelah berhasil membuat proses autentikasi, buatlah proses transfer file. Transfer file dilakukan dari direktori Sender dan dikirim ke direktori Receiver . Proses transfer dilakukan dengan menggunakan perintah TRANSFER filename . Struktur direktorinya akan menjadi seperti berikut:

```
soal 3
-Receiver
-Sender
-receiver.c
-sender.c

```

Karena takut memorinya penuh, Christopher memberi status size(kb) pada setiap pengiriman file yang berhasil.
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".
Catatan:
Dilarang untuk overwrite file users secara permanen
Ketika proses autentikasi gagal program receiver akan mengirim status Authentication failed dan program langsung keluar
Sebelum melakukan transfer file sender harus login (melalui proses auth) terlebih dahulu
Buat transfer file agar tidak duplicate dengan file yang memang sudah ada atau sudah pernah dikirim.
## Penyelesaian
**receiver.c**
```
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
} message;

char *base64decoder(char encoded[], int string_length)
{
    char *decoded_string;
    decoded_string = (char *)malloc(sizeof(char) * 100);

    int i, j, k = 0;

    int num = 0;

    int count_bits = 0;
    for (i = 0; i < string_length; i += 4)
    {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++)
        {
            if (encoded[i + j] != '=')
            {
                num = num << 6;
                count_bits += 6;
            }
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');

            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);

            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);

            else if (encoded[i + j] == '+')
                num = num | 62;

            else if (encoded[i + j] == '/')
                num = num | 63;

            else
            {
                num = num >> 2;
                count_bits -= 2;
            }
        }

        while (count_bits != 0)
        {
            count_bits -= 8;

            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }

    decoded_string[k] = '\0';

    return decoded_string;
}

int main()
{
    key_t key;
    int msgid;
    int authenticated = 0;

    key = ftok("Zulfa", 18);

    msgid = msgget(key, 0666 | IPC_CREAT);

    while (1)
    {
        msgrcv(msgid, &message, sizeof(message), 1, 0);

        message.mesg_text[strcspn(message.mesg_text, "\n")] = '\0';

        if (strcmp(message.mesg_text, "CREDS") == 0)
        {
            FILE *file = fopen("users/users.txt", "r");

            if (file)
            {
                char line[1000];

                while (fgets(line, sizeof(line), file))
                {
                    line[strcspn(line, "\n")] = '\0';

                    char *username = strtok(line, ":");
                    char *encoded_password = strtok(NULL, ":");

                    if (username && encoded_password)
                    {
                        int string_length = strlen(encoded_password);

                        char *decoded_password = base64decoder(encoded_password, string_length);
                        printf("Username: %s, Password: %s\n", username, decoded_password);
                    }
                }
            }
            fclose(file);
        }

        else if (strncmp(message.mesg_text, "AUTH: ", 6) == 0)
        {
            char *auth_message = message.mesg_text + 6;

            char *auth_username = strtok(auth_message, " ");
            char *auth_password = strtok(NULL, "");

            if (auth_username && auth_password)
            {
                int status = 0;
                FILE *file = fopen("users/users.txt", "r");

                if (file)
                {
                    char line[1000];

                    while (fgets(line, sizeof(line), file))
                    {
                        line[strcspn(line, "\n")] = '\0';

                        char *username = strtok(line, ":");
                        char *encoded_password = strtok(NULL, ":");

                        if (username && encoded_password)
                        {
                            int string_length = strlen(encoded_password);
                            char *decoded_password = base64decoder(encoded_password, string_length);

                            if (strcmp(auth_username, username) == 0 && strcmp(auth_password, decoded_password) == 0)
                            {
                                status = 1;
                                break;
                            }

                            free(decoded_password);
                        }
                    }
                    fclose(file);
                }
                if (status)
                {
                    printf("Authentication successful\n");
                    authenticated = 1;
                }
                else
                {
                    printf("Authentication failed\n");
                    authenticated = 0;
                    msgctl(msgid, IPC_RMID, NULL);
                    return 0;
                }
	        }
	    }else{
            printf("UNKNOWN COMMAND\n");
        }
    }
}

```

**penjelasan receiver.c**
```
char *base64decoder(char encoded[], int string_length)
{
    char *decoded_string;
    decoded_string = (char *)malloc(sizeof(char) * 100);

```
Fungsi base64decoder mengambil dua parameter, yaitu encoded (string yang akan didekode) dan string_length (panjang string tersebut).
Fungsi ini mendeklarasikan sebuah pointer decoded_string yang akan digunakan untuk menyimpan hasil dekode.
Dilakukan alokasi memori dinamis untuk decoded_string dengan menggunakan fungsi malloc. Alokasi memori ini akan cukup besar, yaitu 100 byte, untuk menyimpan string hasil dekode.

```
    int i, j, k = 0;
    int num = 0;
    int count_bits = 0;
    for (i = 0; i < string_length; i += 4)
    {
```
Deklarasi variabel i, j, k, num, dan count_bits, yang akan digunakan dalam proses dekode.
Looping for dimulai untuk mengurai string encoded dalam kelompok 4 karakter.

```
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++)
        {
            if (encoded[i + j] != '=')
            {
                num = num << 6;
                count_bits += 6;
            }
```
Di dalam loop for kedua, variabel num dan count_bits diatur ulang untuk setiap grup 4 karakter dalam string Base64.
Setiap karakter di cek, jika bukan tanda "=" , maka num dipindahkan sebanyak 6 bit ke kiri dan count_bits ditambahkan 6.
Ini membantu dalam menggabungkan karakter-karakter Base64 ke dalam num.

```
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);
```
Di dalam loop for ketiga, karakter Base64 yang diubah menjadi nilai numerik dalam rentang 'A' hingga 'Z' dihitung dan digabungkan ke dalam num.

```
            else if (encoded[i + j] == '+')
                num = num | 62;
            else if (encoded[i + j] == '/')
                num = num | 63;
```
Karakter '+' diubah menjadi nilai numerik 62, Karakter '/' diubah menjadi nilai numerik 63 dan digabungkan ke dalam num.

```
        while (count_bits != 0)
        {
            count_bits -= 8;
            decoded_string[k++] = (num >> count_bits) & 255;
        }
```
Setelah pengubahan karakter selesai, sisa bit dalam num diambil dan disimpan dalam decoded_string.

```
    decoded_string[k] = '\0';
    return decoded_string;
}
```
Akhirnya, karakter akhir null ('\0') ditambahkan ke decoded_string untuk menandakan akhir dari string.
Hasil dekode string decoded_string dikembalikan sebagai hasil dari fungsi.

```
key_t key;
int msgid;
int authenticated = 0;
```
Variabel key digunakan untuk menyimpan hasil dari fungsi ftok yang menghasilkan kunci berdasarkan path file dan angka unik. Variabel msgid akan digunakan untuk menyimpan ID pesan dari message queue. Variabel authenticated digunakan untuk menandai status otentikasi (0 untuk belum otentikasi, 1 untuk sudah otentikasi).

```
key = ftok("Zulfa", 18);
```
Fungsi ftok digunakan untuk menghasilkan kunci berdasarkan path file "Zulfa" dan angka unik 18.

```
msgid = msgget(key, 0666 | IPC_CREAT);
```
Fungsi msgget digunakan untuk mendapatkan ID pesan dari message queue. Jika message queue belum ada, maka flag IPC_CREAT digunakan untuk membuatnya. Permission 0666 digunakan untuk memberikan hak akses kepada pengguna dan grup pengguna.

```
while (1)
{
    msgrcv(msgid, &message, sizeof(message), 1, 0);
    message.mesg_text[strcspn(message.mesg_text, "\n")] = '\0';
```
Di dalam loop ini, program akan menunggu pesan yang masuk pada message queue dengan menggunakan fungsi msgrcv. Pesan akan disimpan dalam variabel message.
Setelah menerima pesan, karakter newline ("\n") dalam pesan dihilangkan dengan menggantinya dengan karakter null ('\0') menggunakan fungsi strcspn.

```
if (strcmp(message.mesg_text, "CREDS") == 0)
{
    .........
}
```
Terdapat pengujian kondisi, yaitu apakah isi pesan yang diterima adalah "CREDS" menggunakan fungsi strcmp. Jika pesan adalah "CREDS", maka program akan menjalankan blok kode berikut:

```
FILE *file = fopen("users/users.txt", "r");

            if (file)
            {
                char line[1000];

                while (fgets(line, sizeof(line), file))
                {
                    line[strcspn(line, "\n")] = '\0';

                    char *username = strtok(line, ":");
                    char *encoded_password = strtok(NULL, ":");

                    if (username && encoded_password)
                    {
                        int string_length = strlen(encoded_password);

                        char *decoded_password = base64decoder(encoded_password, string_length);
                        printf("Username: %s, Password: %s\n", username, decoded_password);
                    }
                }
            }
            fclose(file);
```
- char *username = strtok(line, ":");: Kode ini menggunakan fungsi strtok untuk membagi baris yang dibaca menjadi dua bagian, yaitu "username" dan "encoded_password".
- char *encoded_password = strtok(NULL":");: Kode ini menggunakan strtok kembali untuk mengambil bagian kedua dari baris, yaitu "encoded_password". 
- if (username && encoded_password) { ... }: Kode ini melakukan pengujian kondisi untuk memastikan bahwa kedua variabel "username" dan "encoded_password" memiliki nilai yang valid
- int string_length = strlen(encoded_password);: ini menghitung panjang string "encoded_password" untuk digunakan dalam dekripsi base64.
- char *decoded_password = base64decoder(encoded_password, string_length);: Kode ini memanggil fungsi base64decoder untuk mendekripsi password yang terenkripsi dalam base64. Hasil dekripsi disimpan dalam variabel decoded_password. 

```
char *auth_message = message.mesg_text + 6;
```
 Kode ini mengambil pesan setelah "AUTH: " sebanyak 6 karakter.

```
if (auth_username && auth_password) { ... }: 
```
Kode ini melakukan pengujian untuk memastikan bahwa "username" dan "password" memiliki nilai yang valid dalam pesan yang diterima

```
 if (auth_username && auth_password)
            {
                int status = 0;
                FILE *file = fopen("users/users.txt", "r");

                if (file)
                {
                    char line[1000];

                    while (fgets(line, sizeof(line), file))
                    {
                        line[strcspn(line, "\n")] = '\0';

                        char *username = strtok(line, ":");
                        char *encoded_password = strtok(NULL, ":");

                        if (username && encoded_password)
                        {
                            int string_length = strlen(encoded_password);
                            char *decoded_password = base64decoder(encoded_password, string_length);

                            if (strcmp(auth_username, username) == 0 && strcmp(auth_password, decoded_password) == 0)
                            {
                                status = 1;
                                break;
                            }

                            free(decoded_password);
                        }
                    }
                    fclose(file);
                }
                if (status)
                {
                    printf("Authentication successful\n");
                    authenticated = 1;
                }
                else
                {
                    printf("Authentication failed\n");
                    authenticated = 0;
                    msgctl(msgid, IPC_RMID, NULL);
                    return 0;
                }
	        }
	    }else{
            printf("UNKNOWN COMMAND\n");
        }
    }
```
- int status = 0;: 
  Ini mendefinisikan 
  variabel status dengan nilai awal 0. Ini akan digunakan untuk melacak apakah autentikasi berhasil atau gagal.
- char *decoded_password = base64decoder (encoded_password, string_length); 
  Kode ini menggunakan fungsi base64decoder untuk mendekripsi kata sandi terenkripsi dan menghasilkan kata sandi asli dalam bentuk decoded_password.
- if (strcmp(auth_username, username) == 0 && strcmp(auth_password, decoded_password) == 0)     { ... }: 
  Kode ini membandingkan "auth_username" dan "auth_password" dengan "username" dan "decoded_password" dari entri pengguna yang sedang diproses. Jika keduanya cocok, maka autentikasi berhasil dan status diubah menjadi 1. Loop dihentikan dengan break.
- free(decoded_password);: 
  Kode ini membebaskan memori yang dialokasikan untuk decoded_password, karena sudah selesai digunakan.
- if (status) { ... } else { ... }: 
  Kode ini memeriksa nilai status. Jika status adalah 1 (berarti autentikasi berhasil), maka program mencetak "Authentication successful" dan mengeset authenticated menjadi 1. Jika autentikasi gagal (status masih 0), maka program mencetak "Authentication failed", mengeset authenticated menjadi 0, menghentikan pesan antarproses, dan keluar dari program dengan return 0.
- Jika kondisi CREDS dan AUTH tidak terpenuhi, maka kondisi else berikutnya akan dieksekusi, 
  dan program akan mencetak "UNKNOWN COMMAND".

**sender.c**
```
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

struct mesg_buffer{
    long mesg_type;
    char mesg_text[100];
} message;

int main(){
    key_t key;
    int msgid;

    key = ftok("Zulfa", 18);

    msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1){
        perror("msgget");
        exit(1);
    }

    while (1){
        printf("Masukkan pesan: ");
        fgets(message.mesg_text, sizeof(message.mesg_text), stdin);
        message.mesg_text[strcspn(message.mesg_text, "\n")] = '\0';
        message.mesg_type = 1;

        if (msgsnd(msgid, &message, sizeof(message), 0) == -1){
            perror("msgsnd");
            exit(1);
        }

        printf("Pesan terkirim: %s\n", message.mesg_text);
    }
    return 0;
}

```

**penjelasan sender.c**

```
struct mesg_buffer{
    long mesg_type;
    char mesg_text[100];
} message;
```
Kode ini digunakan untuk mengemas pesan yang akan dikirim. Struct ini memiliki dua anggota:
- long mesg_type: Ini adalah jenis pesan, dalam hal ini, diatur menjadi 1.
- char mesg_text[100]: Ini adalah teks pesan yang akan dikirim, dengan panjang maksimum 100 
  karakter.

```
while (1){
        printf("Masukkan pesan: ");
        fgets(message.mesg_text, sizeof(message.mesg_text), stdin);
        message.mesg_text[strcspn(message.mesg_text, "\n")] = '\0';
        message.mesg_type = 1;
```
- Program memasuki loop utama yang akan berjalan tanpa henti.
- Pengguna diminta untuk memasukkan pesan dengan menggunakan printf. Pesan yang dimasukkan  
  oleh pengguna disimpan dalam message.mesg_text. Karakter newline (\n) dihapus dari pesan jika ada.
- Kemudian, jenis pesan (message.mesg_type) diatur menjadi 1, sesuai dengan yang telah
  didefinisikan sebelumnya.

```
if (msgsnd(msgid, &message, sizeof(message), 0) == -1){
            perror("msgsnd");
            exit(1);
        }
```
Pesan dikirimkan menggunakan fungsi msgsnd. Pesan dikemas dalam variabel message dan dikirim ke antrian pesan yang sesuai (ditentukan oleh msgid). Jika pengiriman pesan gagal, program mencetak pesan kesalahan dan keluar. Kemudian, program kembali ke awal loop utama dan menunggu pengguna untuk memasukkan pesan baru.

## Screenshot
- Input Output dari sender dan receiver
![MODUL3SISOP](https://gitlab.com/sylxer/sisop-praktikum-modul-3-2023-mh-it13/-/raw/main/Screenshot/soal_3/MODUL3SISOP.png?ref_type=heads)

## Soal 4
Takumi adalah seorang pekerja magang di perusahaan Evil Corp. Dia mendapatkan tugas untuk membuat sebuah public room chat menggunakan konsep socket. Ketentuan lebih lengkapnya adalah sebagai berikut:<br>
a. Client dan server terhubung melalui socket. <br>
b. Server berfungsi sebagai penerima pesan dari client dan hanya menampilkan pesan saja.  <br>
c. karena resource Evil Corp sedang terbatas buatlah agar server hanya bisa membuat koneksi dengan 5 client saja. <br>

## Penyelesaian
**server.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>

#define MAX_CLIENTS 5
#define MAX_MESSAGE_LENGTH 256

pthread_mutex_t client_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t client_cond = PTHREAD_COND_INITIALIZER;
int num_clients = 0;

void *client_handler(void *arg) {
    int client_socket = *(int *)arg;
    char client_message[MAX_MESSAGE_LENGTH];

    while (1) {
        ssize_t num_bytes = recv(client_socket, client_message, sizeof(client_message) - 1, 0);
        if (num_bytes == -1) {
            perror("Error receiving data from client");
            break;
        } else if (num_bytes == 0) {
            // Connection closed by client
            break;
        }

        client_message[num_bytes] = '\0';
        printf("%s\n", client_message);

        if (strcmp(client_message, "exit") == 0) {
            break;
        }
    }

    pthread_mutex_lock(&client_mutex);
    num_clients--;
    pthread_cond_signal(&client_cond);
    pthread_mutex_unlock(&client_mutex);

    close(client_socket);
    free(arg);
    return NULL;
}

int main() {
    int server_socket, client_socket;
    int *new_client;
    pthread_t client_thread;
    char client_message[MAX_MESSAGE_LENGTH];

    server_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (server_socket == -1) {
        perror("Error creating server socket");
        exit(1);
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(9002);
    server_address.sin_addr.s_addr = INADDR_ANY;

    int reuse = 1;
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1) {
        perror("Error setting socket options");
        exit(1);
    }

    if (bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1) {
        perror("Error binding socket");
        exit(1);
    }

    if (listen(server_socket, MAX_CLIENTS) == -1) {
        perror("Error listening for connections");
        exit(1);
    }

    while (1) {
        client_socket = accept(server_socket, NULL, NULL);
        if (client_socket == -1) {
            perror("Error accepting connection");
        } else {
            pthread_mutex_lock(&client_mutex);
            
            if (num_clients >= MAX_CLIENTS) {
                pthread_mutex_unlock(&client_mutex);
                printf("Aigooo, ini sudah lebih dari 5 client loo. Phei a phei\n");
                close(client_socket);
            } else {
                num_clients++;
                pthread_mutex_unlock(&client_mutex);

                new_client = (int *)malloc(sizeof(int));
                *new_client = client_socket;

                if (pthread_create(&client_thread, NULL, client_handler, (void *)new_client) != 0) {
                    perror("Error creating client thread");
                    free(new_client);
                }
            }
        }
    }

    close(server_socket);
    return 0;
}
```

**Penjelasan code server.c**
Program ini adalah server sederhana yang menggunakan multithreading untuk menerima koneksi dari beberapa klien dan mengelola pesan yang diterima dari klien-klien tersebut. 
```
#define MAX_CLIENTS 5
#define MAX_MESSAGE_LENGTH 256
```
Program mendefinisikan dua macro untuk mengatur batas maksimum jumlah klien yang dapat terhubung (MAX_CLIENTS) dan panjang maksimum pesan yang dapat diterima (MAX_MESSAGE_LENGTH).
```
pthread_mutex_t client_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t client_cond = PTHREAD_COND_INITIALIZER;
int num_clients = 0;
```
Program mendefinisikan mutex (client_mutex) dan condition variable (client_cond) untuk mengkoordinasikan akses ke num_clients, yang digunakan untuk melacak jumlah klien yang terhubung.
```
void *client_handler(void *arg) {
    int client_socket = *(int *)arg;
    char client_message[MAX_MESSAGE_LENGTH];

    while (1) {
        ssize_t num_bytes = recv(client_socket, client_message, sizeof(client_message) - 1, 0);
        if (num_bytes == -1) {
            perror("Error receiving data from client");
            break;
        } else if (num_bytes == 0) {
            // Connection closed by client
            break;
        }

        client_message[num_bytes] = '\0';
        printf("%s\n", client_message);

        if (strcmp(client_message, "exit") == 0) {
            break;
        }
    }

    pthread_mutex_lock(&client_mutex);
    num_clients--;
    pthread_cond_signal(&client_cond);
    pthread_mutex_unlock(&client_mutex);

    close(client_socket);
    free(arg);
    return NULL;
}
```
Fungsi client_handler adalah fungsi yang akan dijalankan dalam thread untuk mengelola koneksi dengan klien. Fungsi ini menerima argumen berupa pointer ke socket klien (arg) dan membaca pesan dari klien. Jika ada kesalahan dalam pembacaan pesan, fungsi mencetak pesan kesalahan. Jika koneksi ditutup oleh klien (num_bytes adalah 0), fungsi keluar dari loop. Pesan dari klien dicetak ke layar.

Jika pesan dari klien adalah "exit", maka koneksi dengan klien akan ditutup dan thread keluar.

Setelah selesai mengelola koneksi dengan klien, num_clients dikurangi, dan condition variable client_cond digunakan untuk memberi tahu bahwa ada klien yang telah selesai. Selanjutnya, socket klien ditutup dan memori yang dialokasikan untuk socket klien tersebut dibebaskan.
```
int main() {
    int server_socket, client_socket;
    int *new_client;
    pthread_t client_thread;
    char client_message[MAX_MESSAGE_LENGTH];
```
Program menginisialisasi beberapa variabel yang akan digunakan dalam program, termasuk socket server (server_socket), socket klien (client_socket), pointer ke socket klien yang baru terhubung (new_client), thread klien (client_thread), dan buffer pesan dari klien (client_message).
```
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
```
Program membuat socket server menggunakan socket(). Socket ini akan digunakan untuk menerima koneksi dari klien.
```
    if (server_socket == -1) {
        perror("Error creating server socket");
        exit(1);
    }
```
Jika pembuatan socket server gagal, program mencetak pesan kesalahan dan keluar dari program.
```
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(9002);
    server_address.sin_addr.s_addr = INADDR_ANY;
```
Program menginisialisasi struktur server_address yang akan digunakan untuk mengikat socket server ke alamat dan port tertentu. Dalam hal ini, server akan menggunakan port 9002.
```
    int reuse = 1;
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1) {
        perror("Error setting socket options");
        exit(1);
    }
```
Program menggunakan setsockopt untuk mengatur socket option SO_REUSEADDR. Ini memungkinkan socket server menggunakan kembali alamat yang sama jika socket sebelumnya ditutup secara normal. Jika pengaturan socket option ini gagal, program mencetak pesan kesalahan dan keluar.
```
    if (bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1) {
        perror("Error binding socket");
        exit(1);
    }
```
Program mengikat socket server ke alamat dan port yang sudah diatur sebelumnya menggunakan bind(). Jika ikatan socket gagal, program mencetak pesan kesalahan dan keluar.
```
    if (listen(server_socket, MAX_CLIENTS) == -1) {
        perror("Error listening for connections");
        exit(1);
    }
```
Program menggunakan listen() untuk mendengarkan koneksi dari klien. Jika mendengarkan gagal, program mencetak pesan kesalahan dan keluar.
```
    while (1) {
        client_socket = accept(server_socket, NULL, NULL);
        if (client_socket == -1) {
            perror("Error accepting connection");
        } else {
            pthread_mutex_lock(&client_mutex);
            
            if (num_clients > MAX_CLIENTS) {
                pthread_mutex_unlock(&client_mutex);
                printf("Aigooo, ini sudah lebih dari 5 client loo. Phei a phei\n");
                close(client_socket);
            } else {
                num_clients++;
                pthread_mutex_unlock(&client_mutex);

                new_client = (int *)malloc(sizeof(int));
                *new_client = client_socket;

                if (pthread_create(&client_thread, NULL, client_handler, (void *)new_client) != 0) {
                    perror("Error creating client thread");
                    free(new_client);
                }
            }
        }
    }

    close(server_socket);
    return 0;
}
```
Program memasuki loop utama, yang akan terus menerima koneksi dari klien. Ketika klien terhubung, program mencoba mengunci client_mutex untuk mengakses num_clients. Jika jumlah klien melebihi batas maksimum (MAX_CLIENTS), program mencetak pesan dan menutup koneksi klien. Jika tidak, program meningkatkan jumlah klien (num_clients), melepas kunci client_mutex, dan membuat thread klien untuk mengelola koneksi dengan klien tersebut. Setelah itu, loop akan kembali ke awal untuk menerima koneksi dari klien berikutnya.
Terakhir, program akan menutup socket server dan mengakhiri program.


**client.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define MAX_MESSAGE_LENGTH 256

int main()
{
    int client_socket;
    char client_message[MAX_MESSAGE_LENGTH];

    // Create client socket
    client_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (client_socket == -1)
    {
        perror("Error creating client socket");
        exit(1);
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(9002);
    server_address.sin_addr.s_addr = INADDR_ANY;

    // Connect to the server
    if (connect(client_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1)
    {
        perror("Error connecting to server");
        exit(1);
    }

    while (1)
    {
        printf("Enter a message: ");
        fgets(client_message, sizeof(client_message), stdin);

        // Remove trailing newline character
        client_message[strcspn(client_message, "\r\n")] = '\0';

        // Check if the user wants to exit
        if (strcmp(client_message, "exit") == 0)
        {
            break;
        }

        // Send the message to the server
        send(client_socket, client_message, strlen(client_message), 0);
    }

    // Close the socket
    close(client_socket);

    return 0;
}
```

**Penjelasan code client.c**
```
int main()
{
    int client_socket;
    char client_message[MAX_MESSAGE_LENGTH];

    // Create client socket
    client_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (client_socket == -1)
    {
        perror("Error creating client socket");
        exit(1);
    }
```
Program menginisialisasi variabel client_socket yang akan digunakan sebagai socket klien. Ini membuat socket klien menggunakan socket(). Jika pembuatan socket gagal (kemungkinan karena alasan tertentu, misalnya gagal alokasi socket), program mencetak pesan kesalahan dan keluar.
```
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(9002);
    server_address.sin_addr.s_addr = INADDR_ANY;
```
Program menginisialisasi struktur server_address yang akan digunakan untuk menentukan alamat dan port server yang akan dihubungi oleh klien. Dalam hal ini, alamat yang digunakan adalah INADDR_ANY yang mengindikasikan bahwa klien akan menghubungi server pada alamat IP lokal.
```
    // Connect to the server
    if (connect(client_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1)
    {
        perror("Error connecting to server");
        exit(1);
    }
```
Program menggunakan connect() untuk menghubungkan socket klien ke alamat server yang telah ditentukan. Jika koneksi gagal (misalnya, server tidak dapat dihubungi), program mencetak pesan kesalahan dan keluar.
```
    while (1)
    {
        printf("Enter a message: ");
        fgets(client_message, sizeof(client_message), stdin);
```
Program memulai loop tak terbatas untuk menerima pesan dari pengguna. Ini mencetak "Enter a message: " ke layar dan kemudian menggunakan fgets() untuk membaca pesan yang dimasukkan oleh pengguna dari stdin (keyboard). Pesan yang dibaca disimpan dalam variabel client_message.
```
        // Remove trailing newline character
        client_message[strcspn(client_message, "\r\n")] = '\0';
```
Program menghapus karakter newline (\n) dari pesan yang dibaca sehingga pesan yang akan dikirim tidak mengandung karakter newline yang tidak diperlukan.
```
        // Check if the user wants to exit
        if (strcmp(client_message, "exit") == 0)
        {
            break;
        }
```
Program memeriksa apakah pesan yang dimasukkan oleh pengguna adalah "exit". Jika ya, program keluar dari loop.
```
        // Send the message to the server
        send(client_socket, client_message, strlen(client_message), 0);
    }
```
Jika pesan yang dimasukkan bukan "exit", program menggunakan send() untuk mengirim pesan tersebut ke server menggunakan socket klien (client_socket).
```
    // Close the socket
    close(client_socket);

    return 0;
}
```
Setelah pengguna memasukkan "exit" atau saat program selesai mengirim pesan, socket klien ditutup dengan close(). Program kemudian keluar dengan nilai kembali 0, menandakan bahwa program selesai.


## Screenshot
- Input Output dari server dan client
![server_client](https://gitlab.com/sylxer/sisop-praktikum-modul-3-2023-mh-it13/-/raw/main/Screenshot/soal_4/server_client.png)

